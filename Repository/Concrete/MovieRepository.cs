﻿using Microsoft.EntityFrameworkCore;
using MoviesAPI.Data;
using MoviesAPI.Models;
using MoviesAPI.Repository.Abstract;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Repository.Concrete
{
    public class MovieRepository : IMovieRepository
    {
        private readonly MoviesAPIDbContext _context;

        public MovieRepository(MoviesAPIDbContext context)
        {
            _context = context;
        }


        /// <summary>
        /// Checks if the movie exists in the table
        /// </summary>
        /// <param name="id">The Id of the movie</param>
        /// <returns>The movie</returns>
        public bool MovieExists(int id)
        {
            return _context.Movies.Any(m => m.Id == id);
        }


        /// <summary>
        /// Creates a movie in the table
        /// </summary>
        /// <param name="movie">The movie to be created</param>
        /// <returns>The movie</returns>
        public async Task<Movie> CreateMovie(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();

            return movie;
        }


        /// <summary>
        /// Deletes a movie from the table by providing a Id
        /// </summary>
        /// <param name="id">The Id of the movie</param>
        /// <returns></returns>
        public async Task DeleteMovie(int id)
        {
            var deleteMovie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(deleteMovie);

            await _context.SaveChangesAsync();
        }


        /// <summary>
        /// Gets all movies from the table
        /// </summary>
        /// <returns>A IEnumerable List with all the movies from the table</returns>
        public async Task<IEnumerable<Movie>> GetAllMovies()
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .ToListAsync();
        }


        /// <summary>
        /// Gets a movie by Id
        /// </summary>
        /// <param name="id">The Id of the movie</param>
        /// <returns>The movie</returns>
        public async Task<Movie> GetMovieById(int id)
        {
            return await _context.Movies
                .Include(m => m.Characters)
                .FirstOrDefaultAsync(m => m.Id == id);
        }

  
        /// <summary>
        /// Updates a movie from the table
        /// </summary>
        /// <param name="movie">The movie to be updated</param>
        /// <returns></returns>
        public async Task UpdateMovie(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }


        /// <summary>
        /// Gets all characters in a movie from the table
        /// </summary>
        /// <param name="id">The Id of the movie</param>
        /// <returns>A IEnumerable List with all the characters</returns>
        public async Task<IEnumerable<Character>> GetCharactersInMovies(int id)
        {
            var movies = await _context.Movies
                 .Include(moive => moive.Characters)
                 .FirstOrDefaultAsync(movies => movies.Id == id);
            return movies.Characters;
        }


        /// <summary>
        /// Updates the character from a movie in the table
        /// </summary>
        /// <param name="id">The Id of the movie to be updated</param>
        /// <param name="characterIds">The ids of the new characters</param>
        /// <returns></returns>
        public async Task UpdateMainCharactersInMovies(int id, int[] characterIds)
        {
            Movie movie = await _context.Movies
                .Include(c => c.Characters)
                .Where(m => m.Id == id)
                .FirstAsync();

            List<Character> characters = new();

            foreach (int characterId in characterIds)
            {
                Character character = await _context.Characters.FindAsync(characterId);
                characters.Add(character);
            }

            movie.Characters = characters;
            await _context.SaveChangesAsync();

        }
    }
}
