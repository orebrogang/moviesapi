﻿using Microsoft.EntityFrameworkCore;
using MoviesAPI.Data;
using MoviesAPI.Models;
using MoviesAPI.Repository.Abstract;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Repository.Concrete
{
    public class FranchiseRepository : IFranchiseRepository
    {

        private readonly MoviesAPIDbContext _context;
        public FranchiseRepository(MoviesAPIDbContext context)
        {
            _context = context;
        }


        /// <summary>
        /// Checks if the franchise exists in the talbe
        /// </summary>
        /// <param name="id">The Id of the franchise</param>
        /// <returns>The franchise</returns>
        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(franchise => franchise.Id == id);
        }


        /// <summary>
        /// Gets a franchise from the table by providing an Id
        /// </summary>
        /// <param name="id">The Id of the franchise</param>
        /// <returns>The franchise</returns>
        public async Task<Franchise> GetFranchiseById(int id)
        {
            return await _context.Franchises.FindAsync(id);
        }


        /// <summary>
        /// Gets all franchises from the table
        /// </summary>
        /// <returns>A IEnumerable List with all the franchises from the table</returns>
        public async Task<IEnumerable<Franchise>> GetAllFranchises()
        {
            return await _context.Franchises.Include(franchise => franchise.Movies).ToListAsync();
        }


        /// <summary>
        /// Adds a franchise to the table
        /// </summary>
        /// <param name="franchise">The franchise that will be added</param>
        /// <returns>The franchise</returns>
        public async Task<Franchise> AddFranchise(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();

            return franchise;
        }


        /// <summary>
        /// Updates a franchise in the table 
        /// </summary>
        /// <param name="franchise">The franchise that will be updated</param>
        /// <returns></returns>
        public async Task UpdateFranchise(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }


        /// <summary>
        /// Deletes a franchise from the table
        /// </summary>
        /// <param name="id">The Id of the franchise to be deleted</param>
        /// <returns></returns>
        public async Task DeleteFranchise(int id)
        {
            var deleteFanchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(deleteFanchise);

            await _context.SaveChangesAsync();
        }


        /// <summary>
        /// Gets all charcters in a franchise
        /// </summary>
        /// <param name="id">The If of the franchise</param>
        /// <returns>A IEnumerable List with all the characters</returns>
        public async Task<IEnumerable<Character>> GetCharactersInFranchise(int id)
        {
            var charactersInFranchise = await GetMoviesInFranchise(id);
            List<Character> characters = new();
            foreach (var movie in charactersInFranchise)
            {
                characters.AddRange(movie.Characters);
            }
            return characters;
        }


        /// <summary>
        /// Gets all movies in a franchise
        /// </summary>
        /// <param name="id">The Id of the franchise</param>
        /// <returns>A IEnumerable List with all the movies in the franchise</returns>
        public async Task<IEnumerable<Movie>> GetMoviesInFranchise(int id)
        {
            var moviesInFranchise = await _context.Movies
                .Include(movie => movie.Characters)
                .Where(movie => movie.FranchiseId == id)
                .ToArrayAsync();
            return moviesInFranchise;
        }


        /// <summary>
        /// Updates a movie in a franchise
        /// </summary>
        /// <param name="id">The Id of the franchise</param>
        /// <param name="movieIds">A list of new movie ids to be added to the franchise</param>
        /// <returns></returns>
        public async Task UpdateMovieInFranchise(int id, int[] movieIds)
        {
            foreach (int movieId in movieIds)
            {
                var movie = await _context.Movies.FindAsync(movieId);
                movie.FranchiseId = id;
            }
            await _context.SaveChangesAsync();
        }
    }
}
