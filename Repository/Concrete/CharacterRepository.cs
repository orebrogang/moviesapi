﻿using Microsoft.EntityFrameworkCore;
using MoviesAPI.Data;
using MoviesAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MoviesAPI.Repository
{
    public class CharacterRepository : ICharacterRepository
    {
        private readonly MoviesAPIDbContext _context;
        public CharacterRepository(MoviesAPIDbContext context)
        {
            _context = context;
        }
        
        /// <summary>
        /// Checks if the character exists in the table
        /// </summary>
        /// <param name="id">The Id of the character</param>
        /// <returns>True of False</returns>
        public bool CharacterExists(int id)
        {
            return _context.Characters.Any(c => c.Id == id);
        }


        /// <summary>
        /// Adds a character to the table
        /// </summary>
        /// <param name="character">The character to be added</param>
        /// <returns>The character that was added</returns>
        public async Task<Character> AddCharacter(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }


        /// <summary>
        /// Deletes a character from the table
        /// </summary>
        /// <param name="id">The Id of the character that will be deleted</param>
        /// <returns></returns>
        public async Task DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
        }


        /// <summary>
        /// Gets all charcaters from the table
        /// </summary>
        /// <returns>A IEnumerable List with all characters</returns>
        public async Task<IEnumerable<Character>> GetAllCharacters()
        {
            return await _context.Characters
                .Include(c => c.Movies).ToListAsync();
        }


        /// <summary>
        /// Gets a character by providing Id
        /// </summary>
        /// <param name="id">The Id of the character</param>
        /// <returns>The character</returns>
        public async Task<Character> GetCharacterById(int id)
        {
            return await _context.Characters
                .Include(c => c.Movies).FirstOrDefaultAsync(c => c.Id == id);
        }


        /// <summary>
        /// Updates a character in the table by providing Id
        /// </summary>
        /// <param name="character">The character to be updated</param>
        /// <returns></returns>
        public async Task UpdateCharacter(Character character)
        {
           if(CharacterExists(character.Id))
            {
            _context.Entry(character).State = EntityState.Modified;
            }
            await _context.SaveChangesAsync();
        }
    }
}
