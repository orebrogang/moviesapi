﻿using MoviesAPI.Models;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MoviesAPI.Repository.Abstract
{
    public interface IMovieRepository
    {
        // Basic CRUD functionality
        public Task<Movie> GetMovieById(int id);
        public Task<IEnumerable<Movie>> GetAllMovies();
        public Task<Movie> CreateMovie(Movie movie);
        public Task UpdateMovie(Movie movie);
        public Task DeleteMovie(int id);

        // Additional functionality
        public Task<IEnumerable<Character>> GetCharactersInMovies(int id);
        public Task UpdateMainCharactersInMovies(int id, int[] characterIds);
        
        // Extra functionality
        public bool MovieExists(int id);

    }
}
