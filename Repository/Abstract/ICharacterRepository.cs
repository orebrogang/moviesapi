﻿using MoviesAPI.Models;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MoviesAPI.Repository
{
    public interface ICharacterRepository
    {
        // Basic CRUD functionality
        public Task <Character> GetCharacterById(int id);
        public Task <IEnumerable<Character>> GetAllCharacters ();
        public Task <Character> AddCharacter(Character character);
        public Task UpdateCharacter (Character character);
        public Task DeleteCharacter (int id);
        
        // Extra functionality
        public bool CharacterExists(int id);

    }
}
