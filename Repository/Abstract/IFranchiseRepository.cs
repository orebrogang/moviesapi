﻿using MoviesAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MoviesAPI.Repository.Abstract
{
    public interface IFranchiseRepository
    {
        // Basic CRUD functionality
        public Task<Franchise> GetFranchiseById(int id);
        public Task<IEnumerable<Franchise>> GetAllFranchises();
        public Task<Franchise> AddFranchise(Franchise franchise);
        public Task UpdateFranchise(Franchise franchise);
        public Task DeleteFranchise(int id);

        // Additional functionality
        public Task UpdateMovieInFranchise(int id, int[] movieIds);
        public Task<IEnumerable<Character>> GetCharactersInFranchise(int id);
        public Task<IEnumerable<Movie>> GetMoviesInFranchise(int id);

        // Extra functionality
        public bool FranchiseExists(int id);
    }
}
