﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoviesAPI.Models
{
    public class Movie
    {
        // Properties
        public int Id { get; set; }

        [Required, MaxLength(50)]
        public string MovieTitle { get; set; }

        [Required, MaxLength(50)]
        public string Genre { get; set; }

        [Required, MaxLength(4)]
        public int ReleaseYear { get; set; }

        [Required, MaxLength(50)]
        public string Director { get; set; }

        [MaxLength(500)]
        public string Picture { get; set; }

        [Url, MaxLength(500)]
        public string Trailer { get; set; }

        // Relationships
        // One movie can have one franchise
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        // One movie can have many characters
        public ICollection<Character> Characters { get; set; }
    }
}
