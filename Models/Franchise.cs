﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MoviesAPI.Models
{
    public class Franchise
    {
        // Properties
        public int Id { get; set; }

        [Required, MaxLength(50)]
        public string Name { get; set; }

        [Required, MaxLength(500)]
        public string Description { get; set; }

        // Relationships
        // One franchise can have many movies
        public ICollection<Movie> Movies { get; set; }
    }
}
