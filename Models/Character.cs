﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoviesAPI.Models
{
    public class Character
    {
        // Properties
        public int Id { get; set; }
        
        [Required, MaxLength(50)]
        public string FullName { get; set; }

        [Required, MaxLength(50)]
        public string Alias { get; set; }

        [Required, MaxLength(50)]
        public string Gender { get; set; }

        [Url, Required, MaxLength(500)]
        public string Picture { get; set; }

        // Relationships
        // One character can have many movies
        public ICollection<Movie> Movies { get; set; }
    }
}
