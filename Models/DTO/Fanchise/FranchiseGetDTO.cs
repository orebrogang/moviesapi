﻿namespace MoviesAPI.Models.DTO.Fanchise
{
    public class FranchiseGetDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
