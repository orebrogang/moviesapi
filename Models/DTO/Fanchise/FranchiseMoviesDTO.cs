﻿namespace MoviesAPI.Models.DTO.Fanchise
{
    public class FranchiseMoviesDTO
    {
        public string MovieTitle { get; set; }
        public int MyProperty { get; set; }
    }
}
