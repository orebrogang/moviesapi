﻿namespace MoviesAPI.Models.DTO.Fanchise
{
    public class FranchiseCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
