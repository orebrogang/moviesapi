﻿namespace MoviesAPI.Models.DTO.Movies
{
    public class MovieUpdateDTO
    {
        public int Id { get; set; }
        public string MovieTitle { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Genre { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public string FranchiseId { get; set; }
    }
}
