﻿using System.Collections.Generic;

namespace MoviesAPI.Models.DTO.Movies
{
    public class MovieGetDTO
    {
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public string Director { get; set; }
        public int ReleaseYear { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public int? FranchiseId { get; set; }
        public List<int> Characters { get; set; }
    }
}
