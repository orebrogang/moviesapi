﻿namespace MoviesAPI.Models.DTO.Movies
{
    public class MovieCharactersDTO
    {
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
    }
}
