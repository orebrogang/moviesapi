﻿using System.Collections.Generic;

namespace MoviesAPI.Models.DTO.Character
{
    public class CharacterGetDTO
    {
        public string FullName { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
        public List<int> Movies { get; set; }
    }
}
