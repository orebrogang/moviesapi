﻿using Microsoft.EntityFrameworkCore;
using MoviesAPI.Models;
using System.Collections.Generic;

namespace MoviesAPI.Data
{
    // Database
    public class MoviesAPIDbContext : DbContext
    {
        // Tables
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MoviesAPIDbContext()
        {

        }

        public MoviesAPIDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed data for the database
            modelBuilder.Entity<Character>()
                .HasData(new Character { Id = 1, FullName = "Bruce Wayne", Alias = "Batman", Gender = "Male", Picture = "Batman.picture" });
            modelBuilder.Entity<Character>()
                .HasData(new Character { Id = 2, FullName = "Clark Kent", Alias = "Superman", Gender = "Male", Picture = "Superman.picture" });
            modelBuilder.Entity<Character>()
            .HasData(new Character { Id = 3, FullName = "Robert Downey Jr", Alias = "Iron Man", Gender = "Male", Picture = "IronMan.picture" });
            modelBuilder.Entity<Character>()
                .HasData(new Character { Id = 4, FullName = "Steve Rogers", Alias = "Captain America", Gender = "Male", Picture = "CaptainAmerica.picture" });
            modelBuilder.Entity<Character>()
               .HasData(new Character { Id = 5, FullName = "Logan", Alias = "Wolverine", Gender = "Male", Picture = "Wolverine.picture" });
            modelBuilder.Entity<Character>()
              .HasData(new Character { Id = 6, FullName = "Unknown", Alias = "Joker", Gender = "Male", Picture = "Joker.picture" });

            modelBuilder.Entity<Movie>()
                .HasData(new Movie { Id = 1, MovieTitle = "Batman Begins", Genre = "Action", ReleaseYear = 2008, Director = "Christopher Nolan", Picture = "Batman.jpg", Trailer = "youtube.com/batmanbegins", FranchiseId = 1 });
            modelBuilder.Entity<Movie>()
                .HasData(new Movie { Id = 2, MovieTitle = "SupermanVsBatman", Genre = "Sci-fi", ReleaseYear = 1500, Director = "Kung Arthur", Picture = "Gooby.jpg", Trailer = "youtube.com/bogsbinny", FranchiseId = 1 });
            modelBuilder.Entity<Movie>()
                .HasData(new Movie { Id = 3, MovieTitle = "The Dark Knight", Genre = "Action", ReleaseYear = 2008, Director = "Christopher Nolan", Picture = "https://upload.wikimedia.org/wikipedia/en/1/1c/The_Dark_Knight_%282008_film%29.jpg", Trailer = "Youtube.com/thedarkknight", FranchiseId = 1 });
            modelBuilder.Entity<Movie>()
               .HasData(new Movie { Id = 4, MovieTitle = "Captain America: Civil War", Genre = "Action", ReleaseYear = 2016, Director = "Joe Anthony Russo", Picture = "https://upload.wikimedia.org/wikipedia/en/1/1c/The_Dark_Knight_%282008_film%29.jpg", Trailer = "Youtube.com/civilWar", FranchiseId = 2 });
            modelBuilder.Entity<Movie>()
              .HasData(new Movie { Id = 5, MovieTitle = "The Wolverine", Genre = "Action", ReleaseYear = 2013, Director = "James Mangold", Picture = "https://upload.wikimedia.org/wikipedia/en/1/1c/The_Dark_Knight_%282008_film%29.jpg", Trailer = "Youtube.com/wolverine", FranchiseId = 2 });
            modelBuilder.Entity<Movie>()
              .HasData(new Movie { Id = 6, MovieTitle = "Joker", Genre = "Action", ReleaseYear = 2019, Director = "Todd Philips", Picture = "https://upload.wikimedia.org/wikipedia/en/1/1c/The_Dark_Knight_%282008_film%29.jpg", Trailer = "Youtube.com/Joker", FranchiseId = 1 });
            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise { Id = 1, Name = "DC Comics", Description = "Story of batman" });
            modelBuilder.Entity<Franchise>()
                .HasData(new Franchise { Id = 2, Name = "Marvel", Description = "Avengers, X-men" });


            // Characters and movies have a many to many relationship
            // Character and movies have a one to many relationship
            // Movie and characters have a one to many relationship
            modelBuilder.Entity<Character>()
                .HasMany(C => C.Movies).WithMany(M => M.Characters).UsingEntity<Dictionary<string, object>>(
                    "MovieCharacters",
                    r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    join =>
                    {
                        join.HasKey("MovieId", "CharacterId");
                        join.HasData(
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 2, CharacterId = 2 },
                            new { MovieId = 4, CharacterId = 3 },
                            new { MovieId = 4, CharacterId = 4 },
                            new { MovieId = 5, CharacterId = 5 },
                            new { MovieId = 3, CharacterId = 6 },
                            new { MovieId = 6, CharacterId = 6 },
                            new { MovieId = 2, CharacterId = 1 }
                            );
                    }
                    );
        }



        // Movie -> characters many to many
        // Movie -> franchise one to many



    }
}
