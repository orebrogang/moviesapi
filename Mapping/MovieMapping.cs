﻿using AutoMapper;
using MoviesAPI.Models;
using MoviesAPI.Models.DTO.Movies;
using System.Linq;

namespace MoviesAPI.Mapping
{
    public class MovieMapping : Profile
    {

        public MovieMapping()
        {
         // Movie > MovieGetDTO
         CreateMap<Movie, MovieGetDTO>()
        .ForMember(movieDto => movieDto.Characters, opt => opt
        .MapFrom(movie => movie.Characters
        .Select(character => character.Id)
        .ToList()));

            // Move > MovieUpdateDTO
            CreateMap<Movie, MovieUpdateDTO>().ReverseMap();
            // Movie > MovieCreateDTO
            CreateMap<MovieCreateDTO, Movie>().ReverseMap();
        }


        
    }
}
