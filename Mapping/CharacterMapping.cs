﻿using AutoMapper;
using MoviesAPI.Models;
using MoviesAPI.Models.DTO.Character;
using System.Linq;

namespace MoviesAPI.Mapping
{
    public class CharacterMapping : Profile
    {
        public CharacterMapping()
        {
            // Character > CharacterGetDTO
            CreateMap<Character, CharacterGetDTO>()
                .ForMember(cDto => cDto.Movies, opt => opt
                .MapFrom(c => c.Movies
                .Select(movie => movie.Id)
                .ToList() 
              )
         );
            // Character > CharacterCreateDTO
            CreateMap<Character, CharacterCreateDTO>().ReverseMap();
            // Character > CharacterUpdateDTO
            CreateMap<Character, CharacterUpdateDTO>().ReverseMap();
        }
    }
}
