﻿using AutoMapper;
using MoviesAPI.Models;
using MoviesAPI.Models.DTO.Fanchise;

namespace MoviesAPI.Mapping
{
    public class FranchiseMapping : Profile
    {
        public FranchiseMapping()
        {
            // Franchise > FranchiseCreateDTO
            CreateMap<Franchise, FranchiseCreateDTO>().ReverseMap();
            // Franchise > FranchiseUpdateDTO
            CreateMap<Franchise, FranchiseUpdateDTO>().ReverseMap();
            // Franchise > FranchiseGetDTO
            CreateMap<Franchise, FranchiseGetDTO>().ReverseMap();
        }

    }
}
