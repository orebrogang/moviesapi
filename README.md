# Creating An API For Movies 

### Using Code First

## Prerequisities

Have SQL-Server Management And Visual Studio 2019 or 2022 installed

## Installation

1. git clone https://gitlab.com/orebrogang/moviesapi.git
2. Run build when starting the project to get the packages thats are needed
3. Swap out the database connection string to your own local one in SQL-Server
4. Run the command "dotnet ef database update" to create the database

## Usage

When you run the program it will start in Swagger where you have many different database calls that can be made, try them out!

## Contributors

### Anders Hansen-Haug
### Alexander Majoros
### Carl Jägerhill
