﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MoviesAPI.Models;
using MoviesAPI.Models.DTO.Character;
using MoviesAPI.Repository;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MoviesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharacterController : ControllerBase
    {
        private readonly ICharacterRepository _repository;
        private readonly IMapper _mapper;

        public CharacterController(ICharacterRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }


        /// <summary>
        /// Get character by Id from the table
        /// </summary>
        /// <param name="id">The Id of the character</param>
        /// <returns>Returns the character</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterGetDTO>> GetCharacterById(int id)
        {
            if(!_repository.CharacterExists(id))
            {
                return NotFound();
            }


            var specificCharacter = await _repository.GetCharacterById(id);
            var specificCharacterDto = _mapper.Map<CharacterGetDTO>(specificCharacter);
            return specificCharacterDto;
        }


        /// <summary>
        /// Gets all the characters from the table
        /// </summary>
        /// <returns>Returns all characters in an IEnumerable List</returns>
        [HttpGet]
        public async Task<IEnumerable<CharacterGetDTO>> GetAllCharacters()
        {
            var allCharacters = await _repository.GetAllCharacters();
            var allcharactersDto = _mapper.Map<List<CharacterGetDTO>>(allCharacters);
            return allcharactersDto;
        }


        /// <summary>
        /// Adds a character to the table with the given values
        /// </summary>
        /// <param name="characterDTO">The character that will be added</param>
        /// <returns>Returns the added character</returns>
        [HttpPost("CreateCharacter")]
        [ActionName(nameof(AddCharacter))]
        public async Task<ActionResult<CharacterCreateDTO>> AddCharacter(CharacterCreateDTO characterDTO)
        {

            var newCharacter = _mapper.Map<Character>(characterDTO);
            newCharacter = await _repository.AddCharacter(newCharacter);
            return CreatedAtAction(nameof(AddCharacter), new {id = newCharacter.Id}, characterDTO);   
        }


        /// <summary>
        /// Updates a character by Id
        /// </summary>
        /// <param name="id">The Id of the character that will be updated</param>
        /// <param name="characterDTO">The new values of the character</param>
        /// <returns>Returns a string for the update</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterUpdateDTO characterDTO)
        {
            if (id != characterDTO.Id)
            {
                return BadRequest();
            }

            if (!_repository.CharacterExists(id)) return NotFound();

            var character = _mapper.Map<Character>(characterDTO);
            await _repository.UpdateCharacter(character);

            return Ok("Character has been updated!");
        }


        /// <summary>
        /// Delete a character from the table using Id
        /// </summary>
        /// <param name="id">The Id of the character</param>
        /// <returns>A string</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_repository.CharacterExists(id))
            {
                return NotFound();
            }

            await _repository.DeleteCharacter(id);

            return Ok("Selected character has been deleted!");
        }

    }
}
