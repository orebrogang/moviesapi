﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MoviesAPI.Models;
using MoviesAPI.Models.DTO.Character;
using MoviesAPI.Models.DTO.Movies;
using MoviesAPI.Repository.Abstract;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MoviesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MovieController : ControllerBase
    {
        private readonly IMovieRepository _repository;
        private readonly IMapper _mapper;

        public MovieController(IMovieRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }


        /// <summary>
        /// Gets a movie from the table by provided Id
        /// </summary>
        /// <param name="id">The id of the movie</param>
        /// <returns>The movie</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieGetDTO>> GetMovieById(int id)
        {
            if (!_repository.MovieExists(id))
            {
                return NotFound($"Couldnt find the movie with the id: {id} *SadFace* *SadFace*");
            }

            var specificMovie = await _repository.GetMovieById(id);
            var specificMovieDto = _mapper.Map<MovieGetDTO>(specificMovie);

            return specificMovieDto;
        }


        /// <summary>
        /// Gets all the movies from the talbe
        /// </summary>
        /// <returns>A IEnumerable List with all the movies</returns>
        [HttpGet]
        public async Task<IEnumerable<MovieGetDTO>> GetAllMovies()
        {
            var allMovies = await _repository.GetAllMovies();
            var allMoviesDto = _mapper.Map<List<MovieGetDTO>>(allMovies);

            return allMoviesDto;
        }


        /// <summary>
        /// Creates a movie on the table
        /// </summary>
        /// <param name="movieDto">The movie to be created</param>
        /// <returns>The movie</returns>
        [HttpPost]
        [ActionName(nameof(CreateMovie))]
        public async Task<ActionResult<MovieCreateDTO>> CreateMovie(MovieCreateDTO movieDto)
        {
            var newMovie = _mapper.Map<Movie>(movieDto);
            newMovie = await _repository.CreateMovie(newMovie);

            return CreatedAtAction(nameof(CreateMovie), new { id = newMovie.Id }, movieDto);
        }


        /// <summary>
        /// Deletes a movie from the table by provided Id
        /// </summary>
        /// <param name="id">The Id of the movie</param>
        /// <returns>A string</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_repository.MovieExists(id))
            {
                return NotFound($"Couldnt find the movie with the id: {id} *SadFace*");
            }

            await _repository.DeleteMovie(id);

            return Ok("Selected movie has been deleted!");
        }


        /// <summary>
        /// Updates a movie from the talbe
        /// </summary>
        /// <param name="id">The Id of the movie to be updated</param>
        /// <param name="updateDto">The new values of the movie</param>
        /// <returns>A string</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateMovie(int id, MovieUpdateDTO updateDto)
        {
            if (id != updateDto.Id)
            {
                return BadRequest("Bad request, try another one!");
            }

            if (!_repository.MovieExists(id))
            {
                return NotFound($"Couldnt find the movie with the id: {id} *SadFace*");
            }

            Movie updateMovie = _mapper.Map<Movie>(updateDto);
            await _repository.UpdateMovie(updateMovie);

            return Ok("The movie has been updated!");
        }


        /// <summary>
        /// Gets the main characters in a movie from the table
        /// </summary>
        /// <param name="id">The Id of the movie</param>
        /// <returns>A IEnumerable List with the characters</returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterGetDTO>>> GetMainCharactersInMovies(int id)
        {
            var charactersInMovie = await _repository.GetCharactersInMovies(id);
            var charactersInMovieDto = _mapper.Map<List<CharacterGetDTO>>(charactersInMovie);
            return charactersInMovieDto;
        }


        /// <summary>
        /// Updates the characters of a movie in the table
        /// </summary>
        /// <param name="id">The Id of the movie that will be updated</param>
        /// <param name="movieId">A list of ids corresponding to the new characters</param>
        /// <returns>A string</returns>
        [HttpPut("{id}/UpdateCharactersInMovie")]
        public async Task<IActionResult> UpdateCharactersInMovie(int id, int[] movieId)
        {
            if (!_repository.MovieExists(id))
            {
                return NotFound($"Couldnt find the movie with the id: {id} *SadFace*");
            }
            await _repository.UpdateMainCharactersInMovies(id, movieId);
            return Ok("Movie was updated with a new character!");
        }
    }     
}

