﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MoviesAPI.Models;
using MoviesAPI.Models.DTO.Character;
using MoviesAPI.Models.DTO.Fanchise;
using MoviesAPI.Models.DTO.Movies;
using MoviesAPI.Repository.Abstract;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MoviesAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchiseController : ControllerBase
    {
        private readonly IFranchiseRepository _repository;
        private readonly IMapper _mapper;

        public FranchiseController(IFranchiseRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }


        /// <summary>
        /// Gets a franchise by Id
        /// </summary>
        /// <param name="id">The Id of the franchise</param>
        /// <returns>The franchise</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseGetDTO>> GetFranchiseById(int id)
        {
            if (!_repository.FranchiseExists(id))
            {
                return NotFound();
            }

            var specificFranchise = await _repository.GetFranchiseById(id);
            return _mapper.Map<FranchiseGetDTO>(specificFranchise);
        }


        /// <summary>
        /// Gets all franchises from the table
        /// </summary>
        /// <returns>The franchises in a IEnumerable List</returns>
        [HttpGet]
        public async Task<IEnumerable<FranchiseGetDTO>> GetAllFranchises()
        {
            var allFranchises = await _repository.GetAllFranchises();
            var allFranchisesDTO = _mapper.Map<List<FranchiseGetDTO>>(allFranchises);
            return allFranchisesDTO;
        }


        /// <summary>
        /// Add franchise to table with given values
        /// </summary>
        /// <param name="franchiseDTO">The franchise that will be added</param>
        /// <returns>The franchise that was added</returns>
        [HttpPost("CreateFranchise")]
        [ActionName(nameof(AddFranchise))]
        public async Task<ActionResult<FranchiseCreateDTO>> AddFranchise(FranchiseCreateDTO franchiseDTO)
        {
            var newFranchise = _mapper.Map<Franchise>(franchiseDTO);
            newFranchise = await _repository.AddFranchise(newFranchise);
            return CreatedAtAction(nameof(AddFranchise), new { id = newFranchise.Id }, franchiseDTO);
        }


        /// <summary>
        /// Updates a franchise by Id
        /// </summary>
        /// <param name="id">Franchise Id</param>
        /// <param name="franchiseDTO">The franchise that will be updated</param>
        /// <returns>A string</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseUpdateDTO franchiseDTO)
        {
            if (id != franchiseDTO.Id)
            {
                return BadRequest();
            }

            if (!_repository.FranchiseExists(id)) return NotFound();

            var franchise = _mapper.Map<Franchise>(franchiseDTO);
            await _repository.UpdateFranchise(franchise);

            return Ok("Character has been updated!");
        }


        /// <summary>
        /// Deletes a franchise with provided Id from the table
        /// </summary>
        /// <param name="id">The Id of the franchise</param>
        /// <returns>A string</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_repository.FranchiseExists(id))
            {
                return NotFound();
            }

            await _repository.DeleteFranchise(id);

            return Ok("Selected character has been deleted!");
        }


        /// <summary>
        /// Gets all movies of a specific franchise
        /// </summary>
        /// <param name="id">The Id of the franchise</param>
        /// <returns>The movies in a IEnumerable List</returns>
        [HttpGet("{id}/GetFranchinseMovies")]
        public async Task<ActionResult<IEnumerable<MovieGetDTO>>> GetMoviesInFranchise(int id)
        {
            var moviesFranchise = await _repository.GetMoviesInFranchise(id);
            var moviesFranchiseDto = _mapper.Map<List<MovieGetDTO>>(moviesFranchise);
            return moviesFranchiseDto;
        }


        /// <summary>
        /// Gets all characters in a franchise with provided Id
        /// </summary>
        /// <param name="id">The id of the franchise</param>
        /// <returns>The characters in a IEnumerable List</returns>
        [HttpGet("{id}/GetCharactersFranchise")]
        public async Task<ActionResult<IEnumerable<CharacterGetDTO>>> GetCharacterInFranchise(int id)
        {
            var characterFranchise = await _repository.GetCharactersInFranchise(id);
            var characterFranchiseDto = _mapper.Map<List<CharacterGetDTO>>(characterFranchise);
            return characterFranchiseDto;
        }


        /// <summary>
        /// Updates a movie by provided Id
        /// </summary>
        /// <param name="id">The Id of the franchise</param>
        /// <param name="movieId">A list of ids correspinding to the new movies</param>
        /// <returns>A string</returns>
        [HttpPut("{id}/UpdateMovieInFranchise")]
        public async Task<IActionResult> UpdateMovieInFranchise(int id, int[] movieId)
        {
            if (!_repository.FranchiseExists(id))
            {
                return NotFound($"Couldnt find the movie with the id: {id} *SadFace*");
            }
            await _repository.UpdateMovieInFranchise(id, movieId);
            return Ok("Movie was updated with a new franchise!");
        }

    }
}
